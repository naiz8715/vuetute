new Vue({
	el: '#vue-app',
	data: {
		name: 'Shuan',
		job: 'Ninja',
		age: 25,
		x: 0,
		y: 0
	},

	methods:{
		greet:function(timeParam){
			return 'Good' + ' ' + timeParam + ' ' + this.name;
		},
		schedule:function(officeParam){
			return this.name + ' ' + 'is working in' + ' ' + officeParam + ' as a ' + this.job;
		},
		address:function(addressParam){
			return this.name + ' ' + 'lives in' + ' ' + addressParam;
		},

		add: function(inc){
			this.age +=inc;
		},
		subtract: function(dec){
			this.age -=dec;
		},
		updateXY: function(event){
		this.x = event.offsetX;
		this.y = event.offsetY;
		}
	}
});